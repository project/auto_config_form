<?php

namespace Drupal\Tests\auto_config_form\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test whether we can install our module without breaking the site.
 *
 * @group auto_config_form
 */
class InstallTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['auto_config_form'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Make sure the site still works.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testLoadFront() {
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Log in');
  }

}
