<?php

namespace Drupal\auto_config_form;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for automatically generating config forms based on a schema.
 */
abstract class AutoConfigFormBase extends ConfigFormBase {

  /**
   * The Typed Config Manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * Create the object and inject dependencies.
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $typed_config_manager = $container->get('config.typed');
    if (!$typed_config_manager) {
      throw new \RuntimeException("Failed to load the Typed Config Manager service.");
    }
    /** @var \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager */
    $instance->typedConfigManager = $typed_config_manager;
    return $instance;
  }

  /**
   * Return the schema key for which a config form should be generated.
   */
  abstract protected function getSchemaKey(): string;

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [$this->getSchemaKey()];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * Get the current config values.
   */
  protected function getImmutableConfig(): ImmutableConfig {
    return $this->configFactory->get($this->getSchemaKey());
  }

  /**
   * Get editable config values. Always WITHOUT overrides.
   */
  protected function getEditableConfig(): Config {
    return $this->config($this->getSchemaKey());
  }

  /**
   * Get a mapping of keys to config definitions from the schema.
   *
   * @returns array[]
   *   An array of config schema definitions.
   */
  protected function getSchemaMapping(): array {
    $mapping = [];
    $schema = $this->typedConfigManager->getDefinition($this->getSchemaKey());
    foreach ($schema['mapping'] as $key => $definition) {
      if (in_array($key, ['langcode', '_core'])) {
        continue;
      }
      $mapping[$key] = $definition;
    }
    return $mapping;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $editableConfig = $this->getEditableConfig();
    $immutableConfig = $this->getImmutableConfig();

    /**
     * @var string $key
     * @var array $definition
     */
    foreach ($this->getSchemaMapping() as $key => $definition) {
      $default_value = $editableConfig->get($key);
      $overridden_value = $immutableConfig->get($key);
      $original_value = $immutableConfig->getOriginal($key, FALSE);

      switch ($definition['type']) {
        case 'string':
          $form[$key] = $this->buildStringElement($definition, $default_value, $overridden_value, $original_value);
          break;

        case 'boolean':
          $form[$key] = $this->buildBooleanElement($definition, $default_value, $overridden_value, $original_value);
          break;

        case 'float':
        case 'integer':
          $form[$key] = $this->buildNumberElement($definition, $default_value, $overridden_value, $original_value);
          break;

        default:
          // Not implemented. Please submit a feature request.
      }
    }

    $form['actions'] = $this->buildActionsElement();
    $form['#theme'] = 'system_config_form';
    return $form;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getEditableConfig();
    foreach ($this->getSchemaMapping() as $key => $definition) {
      $config->set($key, $form_state->getValue($key));
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Build a form element for a 'string' config value.
   */
  protected function buildStringElement(array $definition, $default_value, $overridden_value, $original_value): array {
    return [
      '#type' => 'textfield',
      ...$this->getCommonElementAttributes($definition, $default_value, $overridden_value, $original_value),
    ];
  }

  /**
   * Build a form element for a 'boolean' config value.
   */
  protected function buildBooleanElement(array $definition, $default_value, $overridden_value, $original_value): array {
    return [
      '#type' => 'checkbox',
      ...$this->getCommonElementAttributes($definition, $default_value, $overridden_value, $original_value),
    ];
  }

  /**
   * Build a form element for a 'float' or 'integer' config value.
   */
  protected function buildNumberElement(array $definition, $default_value, $overridden_value, $original_value): array {
    return [
      '#type' => 'number',
      ...$this->getCommonElementAttributes($definition, $default_value, $overridden_value, $original_value),
    ];
  }

  /**
   * Get attributes that are common to all form elements.
   */
  protected function getCommonElementAttributes(array $definition, $default_value, $overridden_value, $original_value): array {
    return [
      '#title' => $definition['title'] ?? NULL,
      '#default_value' => $default_value,
      '#description' => implode("<br>", array_filter([
        $definition['label'] ?? NULL,
        $this->getOverriddenMessage($overridden_value, $original_value),
      ])),
    ];
  }

  /**
   * Build a form element containing the actions (e.g. submit button).
   */
  protected function buildActionsElement(): array {
    return [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save configuration'),
        '#button_type' => 'primary',
      ],
    ];
  }

  /**
   * Get a human-readable override message.
   *
   * @param mixed $overridden
   *   The value after being overridden.
   * @param mixed $original
   *   The value before being overridden.
   *
   * @return string
   *   A human-readable message describing how and why the config value is
   *   overridden.
   */
  protected function getOverriddenMessage($overridden, $original): string {
    return $overridden === $original ? "" : $this->t(
        "This config value is overridden as: <code>@overridden</code>",
        ['@overridden' => $overridden],
      );
  }

}
